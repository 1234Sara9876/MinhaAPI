﻿using MinhaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MinhaAPI.Controllers
{
    public class EmpregadosController : ApiController
    {
        private List<Empregado> Funcionarios;

        /// <summary>
        /// 
        /// </summary>
        public EmpregadosController()
        {
            Funcionarios = new List<Empregado>
            {
                new Empregado {Id=1, Nome="Manel",Apelido="Luz" },
                new Empregado { Id = 2, Nome = "Joana", Apelido = "Marcos" },
                new Empregado { Id = 3, Nome = "Carlos", Apelido = "Barbosa" }
             };
        }

        /// <summary>
        /// Dados dos empregados
        /// </summary>
        /// <returns></returns>
        // GET: api/Empregados
        //retorna a lista com todos os dados dos empregados
        public List<Empregado> Get()
        {
            return Funcionarios;
        }

        /// <summary>
        /// Dados completos de um empregado
        /// </summary>
        /// <param name="id">identificador do empregado</param>
        /// <returns>empregado com o id mencionado</returns>
        // GET: api/Empregados/3
        public Empregado Get(int id)
        {
            return Funcionarios.FirstOrDefault(x => x.Id == id); 
        }

        /// <summary>
        /// Nomes próprios dos empregados
        /// </summary>
        /// <returns>lista com os nomes dos empregados</returns>
        // GET: api/Empregados/GetNomes
        [Route("api/Empregados/GetNomes")]

        public List<string> GetNomes()
        {
            List<string> output = new List<string>();

            foreach (var e in Funcionarios)
            {
                output.Add(e.Nome);
            }
            return output;
        }

        /// <summary>
        /// Registo de novo empregado
        /// </summary>
        /// <param name="valor">Empregado</param>
        // POST: api/Empregados
        public void Post([FromBody]Empregado valor)
        {
            Funcionarios.Add(valor);
        }

        // DELETE: api/Empregados/5
        public void Delete(int id)
        {

        }
    }
}
