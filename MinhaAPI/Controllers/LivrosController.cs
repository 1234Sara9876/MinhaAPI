﻿using MinhaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MinhaAPI.Controllers
{
    public class LivrosController : ApiController
    {
       
        // GET: api/Livros
        public List<Livro> Get()
        {
            return Biblioteca.Livros; 
        }

        // GET: api/Livros/5
        public IHttpActionResult Get(int id)
        {
            Livro livroAchado = Biblioteca.Livros.FirstOrDefault(x=>x.Id==id);
            if (livroAchado!= null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, livroAchado));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,"Livro não localizado"));
        }

        // POST: api/Livros
        public IHttpActionResult Post([FromBody]Livro obj)
        {
            //verificar se n existe já
            Livro livroNovo= Biblioteca.Livros.FirstOrDefault(x => x.Id == obj.Id);
            if (livroNovo != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Conflict, "Esse livro já existe"));
            }
            Biblioteca.Livros.Add(obj);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        // PUT: api/Livros/5
        public IHttpActionResult Put([FromBody]Livro obj)
        {
            Livro livroAAlterar = Biblioteca.Livros.FirstOrDefault(x => x.Id == obj.Id);
            if (livroAAlterar != null)
            {
                livroAAlterar.Titulo = obj.Titulo;
                livroAAlterar.Autor = obj.Autor;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            return ResponseMessage(Request.CreateResponse
            (HttpStatusCode.NotFound,"Livro não localizado"));
        }

        // DELETE: api/Livros/5
        public IHttpActionResult Delete(int id)
        {
            Livro livroAApagar = Biblioteca.Livros.FirstOrDefault(x => x.Id ==id);
            if (livroAApagar != null)
            {
                Biblioteca.Livros.Remove(livroAApagar);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            return ResponseMessage(Request.CreateResponse
            (HttpStatusCode.NotFound, "Livro não localizado"));
        }
    }
}
