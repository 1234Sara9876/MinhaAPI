﻿using MinhaAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MinhaAPI.Controllers
{
    public class CategoriasController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: api/Categorias
        public List<Categoria> Get()
        {
            var lista = from Categoria in dc.Categorias select Categoria;
            return lista.ToList();
        }

        // GET: api/Categorias/AC
        [Route("api/categorias/{sigla}")]
        public IHttpActionResult Get(string sigla)
        {
            var categoria = dc.Categorias.SingleOrDefault (c => c.Sigla == sigla);
            if (categoria != null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, categoria));
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Categoria não existe"));
        }

        // POST: api/Categorias
        public IHttpActionResult Post([FromBody]Categoria novacat)
        {
            Categoria cat = dc.Categorias.FirstOrDefault(c => c.Sigla == novacat.Sigla);
            if (cat != null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Conflict, "Esta categoria já existe"));
            dc.Categorias.InsertOnSubmit(novacat);
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK)); 
        }

        // PUT: api/Categorias
        public IHttpActionResult Put([FromBody]Categoria catAAlterar)
        {
            Categoria cat = dc.Categorias.FirstOrDefault(c => c.Sigla == catAAlterar.Sigla);
            if (cat == null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Conflict, "Essa categoria não existe"));
            cat.Sigla = catAAlterar.Sigla;
            cat.Categoria1 = catAAlterar.Categoria1;

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        // DELETE: api/Categorias/fc
        [Route("api/categorias/{sigla}")]
        public IHttpActionResult Delete(string sigla)
        {
            Categoria cat = dc.Categorias.FirstOrDefault(c => c.Sigla == sigla);
            if (cat!=null)
            {
                dc.Categorias.DeleteOnSubmit(cat);
                try
                {
                    dc.SubmitChanges();
                }
                catch (Exception e)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
                }
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,"Não existe nenhuma categoria com essa sigla"));
        }
    }
}
