﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MinhaAPI.Controllers
{
    public class FilmesController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: api/Filmes
        public List<Filme> Get()
        {
            var lista = from Filme in dc.Filmes select Filme;
            return lista.ToList();
        }

        // GET: api/Filmes/5
        public IHttpActionResult Get(int id)
        {
            var film = dc.Filmes.SingleOrDefault(f => f.Id == id);
            if (film != null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, film));
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "O filme não existe"));
        }

        // POST: api/Filmes
        public IHttpActionResult Post([FromBody]Filme novofilme)
        {
            Filme film = dc.Filmes.FirstOrDefault(f => f.Id == novofilme.Id);
            Categoria cat = dc.Categorias.FirstOrDefault(c => c.Sigla == novofilme.Categoria);
            if (film != null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Conflict, "Este filme já existe"));
            if (cat == null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Categoria inexistente; introduza primeiro a nova categoria"));
            dc.Filmes.InsertOnSubmit(novofilme);
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        // PUT: api/Filmes
        public IHttpActionResult Put([FromBody]Filme filmeAAlterar)
        {
            Filme film = dc.Filmes.FirstOrDefault(f => f.Id == filmeAAlterar.Id);
            Categoria cat = dc.Categorias.FirstOrDefault(c => c.Sigla == filmeAAlterar.Categoria);
            if (film == null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Filme inexistente"));
            if (cat == null)
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Essa categoria não existe; introduza primeiro a nova categoria"));
            film.Id = filmeAAlterar.Id;
            film.Titulo = filmeAAlterar.Titulo;
            film.Categoria = filmeAAlterar.Categoria;

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        // DELETE: api/Categorias/fc
        public IHttpActionResult Delete(int id)
        {
            Filme film = dc.Filmes.FirstOrDefault(f => f.Id == id);
          
            if (film != null)
            {
                dc.Filmes.DeleteOnSubmit(film);
                try
                {
                    dc.SubmitChanges();
                }
                catch (Exception e)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
                }
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Filme inexistente"));
        }
       
    }
}
